var CENTER_IP = '192.168.0.92';
var PORT = '7065';
var processList = ['init', 'vim'];

var child_process = require('child_process');
var http = require('http');
var qs = require('querystring');
var finishFlag = [0, 0];
var obj = {};

start();

function start() {
    exec();
}

function exec() {
    finishFlag = [0, 0];
    info1 = child_process.exec('top -b -n1', infoHandler); 
    info2 = child_process.exec('ifconfig', netInfoHandler);
    setTimeout(exec, 1000);
}

function infoHandler (error, stdout, stderr) {

    // get cpu info
    var cpuStr = stdout.match(/Cpu.*/);
    if (cpuStr) {
        var cpu = [];
        cpu = cpuStr[0].match(/([\d]{1,2}\.[\d]{1,2}\s)/g);
	obj.cpu = cpu;
    }

    // get process info
    var process = [];
    for (var i = 0; i < processList.length; i++) {
        var itemStr = stdout.match(new RegExp(".*" + processList[i] + ".*"));
        if (itemStr) {
            var item = itemStr[0].match(/(\s[\d]{1,2}\.[\d]{1,2})/g);
            var processItem = [];
            processItem.push(processList[i]);
            if (item) {
                processItem.push(item[0]);
                processItem.push(item[1]);
            }
            process.push(processItem);
            obj.process = process;
	}
    }

    finishFlag[0] = 1;
    checkFinish();
}

function netInfoHandler (error, stdout, stderr) {

    // get network info 
    var netStr = stdout.match(/(RX\sbytes:[\d]+\s)|(TX\sbytes:[\d]+\s)/g);
    var netInfo = [];
    if (netStr) {
        var netRXItem = netStr[0].match(/[\d]+/g); 
        var netTXItem = netStr[1].match(/[\d]+/g); 
        obj.network = [netRXItem[0], netTXItem[0]];
    }
    
    finishFlag[1] = 1;
    checkFinish();
}

function checkFinish() {
    var result = 0;
    for (var i = 0; i < finishFlag.length; i++) {
        result += finishFlag[i];
    }
    if (result == finishFlag.length) {
        sendResult();
        finishFlag = [0, 0];
    }
}

function sendResult() {
    obj.name = "dev";
    var json = JSON.stringify(obj);

    var post_data = qs.stringify({
        type : 'text',
        content: json
    });

    var options = {
        host: CENTER_IP,
        port: PORT,
        path: '/server',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': post_data.length
        }
    }

    var reqHttp = http.request(options, function(resHttp) {
        resHttp.setEncoding('utf8');
        resHttp.on('data', function(body1) {
            console.log("body:"+body1);
        });
    });
    reqHttp.write(post_data);
    reqHttp.end();
    reqHttp.on('error', function(e) {
        console.error("error:"+e);
        return e.message;
    });

    console.log(post_data);
}

