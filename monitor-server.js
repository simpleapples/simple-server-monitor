var PORT = 7065;
var DBFILE = './data.db';

var http = require('http');
var qs = require('querystring');
var fs = require('fs');

function writeToFile(file, msg) {
	var data = '{"data":' + msg + ',"timestamp":"' + getStandardDate() + '"}';
	fs.appendFile(file, data + "\r\n");
	console.log(data);
}

function getStandardDate() {
	var a = new Date();	
	return linkTimeStr(
		a.getFullYear() + '-' + 
		linkTimeStr(a.getMonth() + 1) + '-' + 
		linkTimeStr(a.getDate()) + ' ' + 
		linkTimeStr(a.getHours()) + ':' + 
		linkTimeStr(a.getMinutes()) + ':' + 
		linkTimeStr(a.getSeconds()));
}

function linkTimeStr(val) {
	var timeStr;
	if (val < 10) {
		timeStr = '0' + val;
	} else {
		timeStr = '' + val;
	}
	return timeStr;
}

function saveResult(request, response, file) {
	var body = '';
    request.on('data', function (data) {
	    body += data;
	    if (body.length > 1e6) {
            request.connection.destroy();
	    }
	});

	request.on('end', function () {
	    try{
            var result = qs.parse(body);
            writeToFile(file, result.content);
	    } catch(err) {}
	});
	
	response.end("ok");
}

http.createServer(function (request, response) {
	if(request.url.indexOf("/server") == 0) {
		saveResult(request, response, DBFILE);
	} else {
		response.writeHead(404, {'Content-Type': 'text/html'});
	    response.end("page not found.");
	}
}).listen(PORT);

console.log("feedback server run at port: " + PORT);